import { Component, OnInit, Input } from '@angular/core';
import { Order } from '../shared/order';
import { Book } from '../shared/book';
import { Status } from '../shared/status';
import { CardStatus } from '../shared/card-status';
import { OrderService } from "../shared/order.service";
import { BookStoreService } from "../shared/book-store.service";
import { User } from "../shared/user";
import {ListOrder} from "../shared/list-order";

@Component({
  selector: 'bs-order-list',
  templateUrl: './order-list.component.html',
  styles: []
})
export class OrderListComponent implements OnInit {

    @Input() status: Status;

  orders: ListOrder[];
    savedOrders: ListOrder[];
    sorted = false;
  books: Book[];
  users: User[];

  constructor(private os: OrderService, private bs: BookStoreService) { }

  ngOnInit() {
    this.os.getAll().subscribe(res => this.orders = Object.values(res));
      this.os.getUsers().subscribe(res => this.users = res);
    this.bs.getAll().subscribe(res => this.books = res);
  }

    getPriceWithAmount(price: number, amount: number) {
        return (price * amount).toFixed(2);
    }

    getNetto(id: number) {
        var orderspositions: any = [];
        if(this.orders && this.orders[0]) {
            for (let order of this.orders) {
                if (order.id == id) {
                    orderspositions = order.ordersposition;
                }
            }
        }
        var netto = 0;
        if(orderspositions && orderspositions[0]) {
            for (let pos of orderspositions) {
                let x = Number(this.getPriceWithAmount(pos.price, pos.amount));
                netto += x;
            }
        }
        return Number(netto).toFixed(2);
    }

  getBrutto(id: number) {
    var netto = Number(this.getNetto(id));
    var mwst = 0;
    if(this.orders && this.orders[0]) {
        for (let order of this.orders) {
            if (order.id == id) {
                mwst = Number(order.brutto);
            }
        }
    }
    return Number(netto * (1 + (mwst / 100))).toFixed(2);
  }

    getLastStatus(id: number) {
        if(this.orders && this.orders[0]) {
            for (let order of this.orders) {
                if (order.id == id) {
                    return order.statuses[order.statuses.length - 1].bezeichnung;
                }
            }
        }
    }

    send(order_id) {
        const status = new Status(order_id, "versendet", "");

        this.os.saveStatus(status).subscribe(res => location.reload());
    }

    saveState(order_id) {
        const target = event.target as HTMLTextAreaElement;

        const stats = target.previousSibling.previousSibling.firstChild as HTMLInputElement;
        const komment = target.previousSibling.firstChild as HTMLInputElement;

        const status = new Status(order_id, stats.value, komment.value);

        this.os.saveStatus(status).subscribe(res => location.reload());
    }


    editStatus(status) {
        console.log(status);

        var kommentar = prompt("Ändern sie den Kommentar", `${status.kommentar ? status.kommentar : ''}`);

      status = {
          "id": status.id,
          "kommentar": kommentar
      }

        this.os.updateStatus(status).subscribe(res => location.reload());
    }

    sortIdAsc () {
      var sortOrder: any = this.orders;
        sortOrder.sort(function(a, b){
            var keyA = new Date(a.user_id),
                keyB = new Date(b.user_id);
            if(keyA < keyB) return -1;
            if(keyA > keyB) return 1;
            return 0;
        });
    }

    sortIdDesc () {
        var sortOrder: any = this.orders;
        sortOrder.sort(function(a, b){
            var keyA = new Date(a.user_id),
                keyB = new Date(b.user_id);
            if(keyA > keyB) return -1;
            if(keyA < keyB) return 1;
            return 0;
        });
    }

    sortDateAsc () {
        var sortOrder: any = this.orders;
        sortOrder.sort(function(a, b){
            var keyA = new Date(a.created_at),
                keyB = new Date(b.created_at);
            if(keyA < keyB) return -1;
            if(keyA > keyB) return 1;
            return 0;
        });
    }

    sortDateDesc () {
        var sortOrder: any = this.orders;
        sortOrder.sort(function(a, b){
            var keyA = new Date(a.created_at),
                keyB = new Date(b.created_at);
            if(keyA > keyB) return -1;
            if(keyA < keyB) return 1;
            return 0;
        });
    }

    sortStatus (word: string) {
      if (this.sorted) {
          this.orders = this.savedOrders;
      } else {
          this.savedOrders = this.orders;
      }

        let sortedOrders = [];
        for (let order of this.orders) {
            if (order.statuses[order.statuses.length - 1].bezeichnung == word) {
                sortedOrders.push(order);
            }

            /*for (let status of order.statuses) {
                if (status.bezeichnung == word) {
                    sortedOrders.push(order);
                }
            }*/
        }
        this.sorted = true;
        this.orders = sortedOrders;
    }

    sortWithoutStatus () {
      if (this.sorted) {
          this.orders = this.savedOrders;
      }
    }

    getTitle (id: number) {
        var title = '-';
        if (this.books && this.books[0] ) {
            for (let book of this.books) {
                if (book.id == id) {
                    if(book.title) {
                        title = book.title;
                    }
                }
            }
        }
        return title;
    }

    getSubtitle (id: number) {
        var subtitle = '-';
        if (this.books && this.books[0] ) {
            for (let book of this.books) {
                if (book.id == id) {
                    if(book.subtitle) {
                        subtitle = book.subtitle;
                    }
                }
            }
        }
        return subtitle;
    }

    getISBN (id: number) {
        var isbn = '-';
        if (this.books && this.books[0] ) {
            for (let book of this.books) {
                if (book.id == id) {
                    if(book.isbn) {
                        isbn = book.isbn;
                    }
                }
            }
        }
        return isbn;
    }

    getAddress (id: number) {
      if (this.users && this.users[0]) {
          for (let user of this.users) {
              if (user.id == id && user.address.country && user.address.location && user.address.street) {
                  return user.address.country + ' - ' + user.address.location + ', ' + user.address.street;
              }
          }
      }
    }
}
