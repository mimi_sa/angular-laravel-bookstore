import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Book} from '../shared/book';
import {ActivatedRoute, Router} from "@angular/router";
import {BookStoreService} from "../shared/book-store.service";
import {AuthService} from "../shared/authentication.service";

@Component({
    selector: 'bs-book-details',
    templateUrl: './book-details.component.html',
    styles: []
})
export class BookDetailsComponent implements OnInit {

    @Input() book: Book;
    //@Output() showListEvent = new EventEmitter<any>();

    //Dependency Injection , Zu verfügung stellen
    constructor(private bs: BookStoreService,
                private router: Router,
                private route: ActivatedRoute,
                private authService: AuthService) {

    }

    ngOnInit() {
        const params = this.route.snapshot.params;
        //this.book = this.bs.getSingle(params['isbn']);
        this.bs.getSingle(params['isbn']).subscribe(
            //verkürzte lambda schreibweise von ES6 für function die datafromobservable übergibt
            datafromObservable => this.book = datafromObservable
        );
    }

    removeBook() {
        if (confirm("Buch wirklich löschen?")) {
            this.bs.remove(this.book.isbn)
                .subscribe(res => this.router.navigate(['../'],
                    {relativeTo: this.route}
                ));
        }
    }

    getRating(num: number) {
        return new Array(num);
    }

    save() {
        var same = false;
        var i = 0;
        if (!localStorage.getItem('books')) {
            var books = {
                queue: []
            };
            localStorage.setItem('books', JSON.stringify(books));
        }

        var restoredBooks = JSON.parse(localStorage.getItem('books'));

        for (let book of restoredBooks.queue) {
            if (!same && book.id == this.book.id) {
                restoredBooks.queue[i].amount = book.amount + 1;
                same = true;

                document.getElementById("pop-up-notsame").style.visibility='visible';
                var timeoutID = window.setTimeout(() => {
                    document.getElementById("pop-up-notsame").style.visibility='hidden';
                }, 3000);
            }
            i++;
        }

        if (!same) {
            restoredBooks.queue.push({
                id:  this.book.id,
                title: this.book.title,
                subtitle: this.book.subtitle,
                isbn: this.book.isbn,
                price: this.book.price,
                amount: 1,
            });

            document.getElementById("pop-up-same").style.visibility='visible';
            var timeoutID = window.setTimeout(() => {
                document.getElementById("pop-up-same").style.visibility='hidden';
            }, 3000);
        }

        localStorage.setItem('books', JSON.stringify(restoredBooks));


    }

    /*
    showBookList(){
      this.showListEvent.emit();
    }*/

}
