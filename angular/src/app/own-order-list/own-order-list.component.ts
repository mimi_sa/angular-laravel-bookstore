import { Component, OnInit, Input } from '@angular/core';
import { Order } from '../shared/order';
import { Book } from '../shared/book';
import {ActivatedRoute, Router} from "@angular/router";
import { OrderService } from "../shared/order.service";
import { BookStoreService } from "../shared/book-store.service";
import { Status } from '../shared/status';
import {Ordersposition} from "../shared/ordersposition";
import {User} from "../shared/user";
import {ListOrder} from "../shared/list-order";

@Component({
  selector: 'bs-own-order-list',
  templateUrl: './own-order-list.component.html',
  styles: []
})
export class OwnOrderListComponent implements OnInit {

    @Input() order: ListOrder;
    @Input() ordersposition: Ordersposition;

    orders: ListOrder[];
    books: Book[];
    users: User[];

  constructor(private os: OrderService, private bs: BookStoreService, private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
      this.os.getOwn().subscribe(res => this.orders = res);
      this.os.getUsers().subscribe(res => this.users = res);
      this.bs.getAll().subscribe(res => this.books = res);
  }

    getPriceWithAmount(price: number, amount: number) {
        return (price * amount).toFixed(2);
    }

    getNetto(id: number) {
        var orderspositions: any = [];
        if(this.orders && this.orders[0]) {
            for (let order of this.orders) {
                if (order.id == id) {
                    orderspositions = order.ordersposition;
                }
            }
        }
        var netto = 0;
        if(orderspositions && orderspositions[0]) {
            for (let pos of orderspositions) {
                let x = Number(this.getPriceWithAmount(pos.price, pos.amount));
                netto += x;
            }
        }
        return Number(netto).toFixed(2);
    }

    getBrutto(id: number) {
        var netto = Number(this.getNetto(id));
        var mwst = 0;
        if(this.orders && this.orders[0]) {
            for (let order of this.orders) {
                if (order.id == id) {
                    mwst = Number(order.brutto);
                }
            }
        }
        return Number(netto * (1 + (mwst / 100))).toFixed(2);
    }

    getLastStatus(id: number) {
        if(this.orders && this.orders[0]) {
            for (let order of this.orders) {
                if (order.id == id) {
                    return order.statuses[order.statuses.length - 1]["bezeichnung"];
                }
            }
        }
    }

    bezahlen(id: number) {
      const status = new Status(id, "bezahlt", "");

      this.os.saveStatus(status).subscribe(res => location.reload());
    }

    removeOrder(id: number) {
        if (confirm("Bestellung wirklich löschen?")) {
            this.os.removeOrder(id)
                .subscribe(res => location.reload());
        }
    }

    storno(id: number) {
        const status = new Status(id, "storniert", "");

        this.os.saveStatus(status).subscribe(res => location.reload());
    }

    editOrdersposition(ordersposition) {
        var amount = prompt("Stückzahl ändern", `${ordersposition.amount ? ordersposition.amount : '1'}`);
        if (parseInt(amount) > 0 && parseInt(amount) < 100000) {
            this.ordersposition = {
                "id": ordersposition.id,
                "amount": Number(amount),
            };
            this.os.updateOrdersposition(this.ordersposition).subscribe(res => location.reload());
        }
    }

    deleteOrdersposition(ordersposition) {
        var oneInOrder = false;
        for (let order of this.orders) {
            if (order.id == ordersposition.order_id) {
                if (order.ordersposition.length == 1) {
                    this.removeOrder(order.id);
                    oneInOrder = true;
                }
            }
        }

        if (!oneInOrder) {
            if (confirm("Bestellposition wirklich löschen?")) {
                this.os.removeOrdersposition(ordersposition.id)
                    .subscribe(res => location.reload());
            }
        }
    }

    getTitle (id: number) {
      var title = '-';
      if (this.books && this.books[0] ) {
          for (let book of this.books) {
              if (book.id == id) {
                  if(book.title) {
                      title = book.title;
                  }
              }
          }
      }
      return title;
    }

    getSubtitle (id: number) {
        var subtitle = '-';
        if (this.books && this.books[0] ) {
            for (let book of this.books) {
                if (book.id == id) {
                    if(book.subtitle) {
                        subtitle = book.subtitle;
                    }
                }
            }
        }
        return subtitle;
    }

    getISBN (id: number) {
        var isbn = '-';
        if (this.books && this.books[0] ) {
            for (let book of this.books) {
                if (book.id == id) {
                    if(book.isbn) {
                        isbn = book.isbn;
                    }
                }
            }
        }
        return isbn;
    }

    getAddress (id: number) {
        if (this.users && this.users[0]) {
            for (let user of this.users) {
                if (user.id == id && user.address.country && user.address.location && user.address.street) {
                    return user.address.country + ' - ' + user.address.location + ', ' + user.address.street;
                }
            }
        }
    }
}
