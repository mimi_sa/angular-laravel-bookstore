import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from "@angular/forms";
import { Router} from "@angular/router";
import { AuthService } from "../../shared/authentication.service";

interface Response {
    response : string;
    result: {
        token: string;
    }
}

@Component({
    selector: 'bs-login',
    templateUrl: './login.component.html',
    styles: []
})
export class LoginComponent implements OnInit {

    loginForm : FormGroup;

    constructor( private fb: FormBuilder, private router: Router,
                 private authService: AuthService) { }



    ngOnInit() {
        this.loginForm = this.fb.group({
            username: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });
    }

    login() {
        //wenn ich beim Formular auf abschicken drucke wird diese Methode aufgerufen
        //ich übermittle Username und Password an authentificationService
        const val = this.loginForm.value;
        if (val.username && val.password) {
            //authService wird aufgerufen. wenn username/pw an REST Service geschickt wurde, kann ich schaun ob wir eingeloggt sind od nicht
            this.authService.login(val.username, val.password).subscribe(res => {
                const resObj = res as Response; //mach typemapping
                if (resObj.response === 'success') {
                    //wenn Key success ist (Antwort json zb postman)
                    this.authService.setLocalStorage(resObj.result.token); //local Storage erwartet Token
                    this.router.navigateByUrl(('/'));
                }
            })
        }
    }

    isLoggedIn() {
        return this.authService.isLoggedIn();
    }

    logout() {
        this.authService.logout();
    }

}
