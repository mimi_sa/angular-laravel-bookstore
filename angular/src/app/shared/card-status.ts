export class CardStatus {
    constructor (
        public bezeichnung: string,
        public kommentar?: string
    ) {}
}
