import {Ordersposition} from "./ordersposition";
import {Status} from "./status";

export class ListOrder {
    constructor (
        public id: number,
        public user_id: number,
        public brutto: string | number,
        public statuses? : Status[],
        public ordersposition? : Ordersposition[]
    ) {}

}
