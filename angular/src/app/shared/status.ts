export class Status {
    constructor (
        public order_id: number,
       public bezeichnung: string,
       public kommentar?: string
    ) {}
}
