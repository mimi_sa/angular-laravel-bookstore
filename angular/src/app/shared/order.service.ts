import { Injectable } from '@angular/core';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";
import {Status, Order, Ordersposition} from "./order";
import { Tax } from "./tax";
import { User } from "./user";
import {ListOrder} from "./list-order";
import {StatusUpdate} from "./status-update";

@Injectable()
export class OrderService {

    private api = 'http://bookstore19.s1610456029.student.kwmhgb.at/api';


    constructor(private http: HttpClient) { }

    getAll() : Observable<Array<ListOrder>> {
        return this.http.get(`${this.api}/orders`)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    getOwn() : Observable<Array<ListOrder>> {
        return this.http.get(`${this.api}/ownOrder`)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    create (order: Order) : Observable<any> {
        return this.http.post(`${this.api}/card`, order)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    update (order: ListOrder) : Observable<any> {
        return this.http.put(`${this.api}/order/${order.id}`, order)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    updateOrdersposition (ordersposition: Ordersposition) : Observable<any> {
        return this.http.put(`${this.api}/orderposition/${ordersposition.id}`, ordersposition)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    removeOrdersposition (id: string) : Observable<any> {
        return this.http.delete(`${this.api}/orderposition/${id}`)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    remove (id: string) : Observable<any>  {
        return this.http.delete(`${this.api}/order/${id}`)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    saveStatus (status: Status) : Observable<any> {
        return this.http.post(`${this.api}/statuses`, status)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    updateStatus (status: StatusUpdate) : Observable<any> {
        return this.http.put(`${this.api}/statuses/${status.id}`, status)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    removeStatus (id: string) : Observable<any>  {
        return this.http.delete(`${this.api}/status/${id}`)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    getUsers() : Observable<Array<User>> {
        return this.http.get(`${this.api}/users`)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    getTax() : Observable<Tax> {
        return this.http.get(`${this.api}/tax`)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    removeOrder (id: number): Observable<any>  {
        return this.http.delete(`${this.api}/order/${id}`)
            .pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    private errorHandler ( error:Error | any) : Observable<any> {
        return throwError(error);
    }
}
