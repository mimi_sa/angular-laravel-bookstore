<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['api', 'cors']], function() {
    Route::post('auth/login', 'Auth\ApiAuthController@login');

    Route::get('books', 'BookController@index');
    Route::get('book/{isbn}', 'BookController@findByISBN');
    Route::get('book/checkisbn/{isbn}', 'BookController@checkISBN');
    Route::get('books/search/{searchTerm}', 'BookController@findBySearchTerm');
    });


// methods with auth
Route::group(['middleware' => ['api', 'cors', 'jwt-auth']], function() {
    Route::post('order', 'OrderController@saveBrutto');

    Route::get('ownOrder', 'OrderController@getOwnOrders');
    Route::get('orderposition/{order_id}', 'OrderController@getOrdersposition');
    Route::delete('order/{order_id}', 'OrderController@deleteOrder');
    Route::delete('orderposition/{id}', 'OrderController@deleteOrdersposition');
    Route::delete('status/{id}', 'OrderController@deleteStatus');
    Route::get('users', 'UserController@getAllUsers');
    Route::get('tax', 'OrderController@getTax');

    Route::post('statuses', 'OrderController@saveStatus');
    Route::put('statuses/{id}', 'OrderController@updateStatus');
    Route::put('order/{id}', 'OrderController@updateOrder');
    Route::post('card', 'OrderController@saveOrder');
    Route::post('orderposition', 'OrderController@saveOrderposition');
    Route::put('orderposition/{id}', 'OrderController@updateOrderposition');

    //ADMIN !!!!!!!!!
    Route::get('orders', 'OrderController@getAllOrders');

// update book
    Route::put('book/{isbn}', 'BookController@update');
// delete book
    Route::delete('book/{isbn}', 'BookController@delete');


// insert book
    Route::post('book', 'BookController@save');

    Route::post('auth/logout', 'Auth\ApiAuthController@logout');
});

