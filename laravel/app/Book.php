<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Book extends Model
{
    // alle beschreibbaren properties
    protected $fillable = [
        'isbn', 'title', 'subtitle', 'published',
        'rating', 'description', 'price', 'invisible'
    ];

    public function isFavourite() : bool {
        return $this->rating > 5;
    }

    public function images() : HasMany {
        return $this->hasMany(Image::class);
    }

    public function orderspositions() : HasMany {
        return $this->hasMany(Ordersposition::class);
    }

    public function authors() : BelongsToMany {
        return $this->belongsToMany(Author::class)->withTimestamps();
    }
}
