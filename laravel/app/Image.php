<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Book;

class Image extends Model
{
    protected $fillable = [
        'url', 'title'
    ];

    //belongsto abbildung einer relation - siehe eloquent doku
    //das bild gehört zu einem buch, ein buch hat viele bilder
    public function book() : BelongsTo{
        return $this->belongsTo(Book::class);
    }

}
