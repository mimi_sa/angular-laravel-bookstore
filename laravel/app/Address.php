<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Address extends Model
{
    protected $fillable = [
        'country', 'plz', 'location', 'street', 'user_id'
    ];

    /**
     * Get the user that has these address
     */
    public function user() : BelongsTo {
        return $this->belongsTo(User::class);
    }

    /*public function tax() : BelongsTo {
        return $this->belongsTo(Tax::class);
    }*/
}
