<?php

use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $a1 = new \App\Author();
        $a1->firstName = "John ";
        $a1->lastName = "Tolkien";
        $a1->save();

        $a2 = new \App\Author();
        $a2->firstName = "George";
        $a2->lastName = "Martin";
        $a2->save();

        $a3 = new \App\Author();
        $a3->firstName = "Joanne";
        $a3->lastName = "Rowling";
        $a3->save();

        $a4 = new \App\Author();
        $a4->firstName = "Phillip";
        $a4->lastName = "Peterson";
        $a4->save();

        $a5 = new \App\Author();
        $a5->firstName = "Adrian";
        $a5->lastName = "Tchaikovsky";
        $a5->save();

        $a6 = new \App\Author();
        $a6->firstName = "Brian";
        $a6->lastName = "Aldiss";
        $a6->save();

        $a7 = new \App\Author();
        $a7->firstName = "Cecila";
        $a7->lastName = "Ahern";
        $a7->save();

        $a8 = new \App\Author();
        $a8->firstName = "Bas";
        $a8->lastName = "Kast";
        $a8->save();

        $a9 = new \App\Author();
        $a9->firstName = "Walter";
        $a9->lastName = "Moers";
        $a9->save();
    }
}
